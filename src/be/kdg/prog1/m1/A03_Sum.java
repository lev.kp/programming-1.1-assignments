package be.kdg.prog1.m1;

import java.util.Scanner;

public class A03_Sum {

    public static void main(String[] args) {
        int first, second;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a number: ");
        first = sc.nextInt();

        System.out.println("Enter another number");
        second = sc.nextInt();

        System.out.println("The sum is: " + (first + second));

        sc.close();
    }
}