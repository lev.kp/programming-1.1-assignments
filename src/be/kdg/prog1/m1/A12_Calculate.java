package be.kdg.prog1.m1;

import java.util.Scanner;
import java.util.Scanner;

public class A12_Calculate {

    public static void main(String[] args) {

        int num1, num2, operationIndex;
        int exponentiationResult = 1;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a number: ");
        num1 = sc.nextInt();

        System.out.println("Enter another number: ");
        num2 = sc.nextInt();

        System.out.println("Choose an operation: \n1. add\n2. subtract\n3. multiply\n4. divide\n5. exponentiation");

        operationIndex = sc.nextInt();
        System.out.println("Your choice: " + operationIndex);

        //or use switch for this:
        if (operationIndex == 1) {
            System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
        }
        else if(operationIndex == 2) {
            System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
        }
        else if(operationIndex == 3) {
            System.out.println(num1 + " * " + num2 + " = " + (num1 * num2));
        }
        else if(operationIndex == 4) {
            System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));
        }
        else if(operationIndex == 5) {

            if (num2 != 0) {
                System.out.print(num1 + "^" + num2 + " = ");
            }
            else {
                System.out.print(num1 + "^" + num2 + " ");
            }

            for (int i = 0; i < num2; i++) {
                exponentiationResult *= num1;
                System.out.print(num1 + " ");

                if (i != (num2 - 1)) {
                    System.out.print("* ");
                }
            }
            System.out.print("= " + exponentiationResult);
        }
        else {
            System.out.println("Choose from 1 to 5.");
        }

        sc.close();
    }
}