package be.kdg.prog1.m1;

import java.util.Scanner;

public class A09_BMI_v2 {

    public static void main(String[] args) {

        double weight = 0;
        double height = 0;
        double bmi;
        Scanner sc = new Scanner(System.in);

        System.out.println("Dear patient, this program will calculate your BMI.");
        System.out.println("Enter your weight in kilograms: ");
        weight = sc.nextDouble();
        System.out.println("Enter your height in meters:");
        height = sc.nextDouble();

        bmi = weight / (height * height);
        System.out.println("Your BMI is: " + bmi);

        if (bmi < 18) {
            System.out.println("underweight");
        }
        else if (bmi > 18 && bmi < 25) {
            System.out.println("healthy weight");
        }
        else if (bmi >= 25 && bmi < 30) {
            System.out.println("overweight");
        }
        else {
            System.out.println("obese");
        }

        sc.close();
    }
}
