package be.kdg.prog1.m1;

import java.util.Scanner;
import java.lang.Math; //for the Math.min and Math.max functions

public class A13_Between {
    public static void main(String[] args) {

        int num1, num2, num3, median;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Enter the first number (1..100):");
            num1 = sc.nextInt();
        } while(!isNumberInRange(num1));

        do {
            System.out.println("Enter the second number (1..100):");
            num2 = sc.nextInt();
        } while(!isNumberInRange(num2));

        do {
            System.out.println("Enter the third number (1..100):");
            num3 = sc.nextInt();
        } while(!isNumberInRange(num3));
        
        if (num1 == num2 && num1 == num3) {
            System.out.println("The median is: " + num1);
        }

        median = Math.max(Math.min(num1,num2), Math.min(Math.max(num1,num2),num3));
        System.out.println("The middle number is: " + median);

        sc.close();
    }

    static boolean isNumberInRange(int num) {
        if (num < 1 || num > 100) {
            System.out.println("The number should be between 1 and 100!");
            return false;
        }
        return true;
    }
}