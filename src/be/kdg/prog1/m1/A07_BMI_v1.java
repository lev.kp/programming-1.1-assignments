package be.kdg.prog1.m1;

import java.util.Scanner;

public class A07_BMI_v1 {

    public static void main(String[] args) {
        double weight, height;
        Scanner sc = new Scanner(System.in);

        System.out.println("Dear patient, this program will calculate your BMI.");
        System.out.println("Enter your weight in kilograms: ");
        weight = sc.nextDouble();
        System.out.println("Enter your height in meters:");
        height = sc.nextDouble();

        System.out.println("Your BMI is: " + (weight / (height * height)));

        sc.close();
    }
}
