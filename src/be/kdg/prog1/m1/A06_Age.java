package be.kdg.prog1.m1;

import java.util.Scanner;
import java.time.LocalDate;

public class A06_Age {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name;
        int birthYear;

        System.out.println("Please enter your name: ");
        name = sc.nextLine();

        System.out.println("Dear " + name + ", please enter the year you were born: ");
        birthYear = sc.nextInt();

        System.out.println("If all goes well, you'll be " + (LocalDate.now().getYear() - birthYear) + " by the end of the year.");

        sc.close();
    }
}
