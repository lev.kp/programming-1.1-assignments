package be.kdg.prog1.m5.a03box;

public class Box {
    private String type;
    private double length;
    private double width;
    private double height;

    public Box(String type, double length, double width, double height) {
        this.type = type;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public Box(String type, double length) {
        this.type = type;
        this.length = length;
        this.width = length;
        this.height= length;
    }

    public double surface() {
        return  2 * (height * width + height * length + width * length);
    }

    public double volume() {
        return length * width * height;
    }

    public double tapeLength() {
        return height + width * 2;
    }

    public String toString() {
        return String.format(
                "Type: %s\n" +
                        "Length: %.1f cm\n" +
                        "Width: %.1f cm\n" +
                        "Height: %.1f cm\n" +
                        "Surface: %.1f cm²\n" +
                        "Volume: %.1f cm³\n" +
                        "Minimum tape length: %.1f cm\n", type, length, width, height, surface(), volume(), tapeLength()
                );
    }
}