package be.kdg.prog1.m5.a06salesperson;

import java.util.Scanner;

public class TestSalesPerson {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String name;
        double revenue ;

        name="Jan";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson one = new SalesPerson(name, scanner.nextDouble());

        name="Laetitia";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson two = new SalesPerson(name, scanner.nextDouble());

        name="Lotte";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson three = new SalesPerson(name, scanner.nextDouble());

        System.out.println("Our top earner is " + SalesPerson.topEarner(one, two, three).toString() + "!");
    }
}
