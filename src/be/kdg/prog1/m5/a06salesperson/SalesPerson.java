package be.kdg.prog1.m5.a06salesperson;

public class SalesPerson {

    private String name;
    private double revenue;

    public SalesPerson(String name, double revenue) {
        this.name = name;
        this.revenue = revenue;
    }

    public String getName() {
        return name;
    }

    public double getRevenue() {
        return revenue;
    }

    public boolean hasMoreRevenueThan(SalesPerson other) {
        return (this.getRevenue() > other.getRevenue() ? true : false);
    }

    public String toString() {
        return getName();
    }

    public static SalesPerson topEarner(SalesPerson sp_1, SalesPerson sp_2, SalesPerson sp_3) {

        //double maxRevenue = Math.max(Math.max(sp_1.getRevenue(), sp_2.getRevenue()), sp_3.getRevenue());

        if (sp_1.hasMoreRevenueThan(sp_2) && sp_1.hasMoreRevenueThan(sp_3)) {
            return sp_1;
        }
        else if (sp_2.hasMoreRevenueThan(sp_1) && sp_2.hasMoreRevenueThan(sp_3)) {
            return sp_2;
        }
        else {
            return  sp_3;
        }
    }
}