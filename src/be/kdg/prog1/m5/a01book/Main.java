package be.kdg.prog1.m5.a01book;

public class Main {
    public static void main(String[] args) {

        Book a = new Book("Deitel & Deitel", "Java, How to Program", 1186);
        Book b = new Book("Hillary Mantel", "Wolf Hall", 604);
        Book c = new Book("P.G. Wodehouse", "Leave it to Psmith", 352);

        a.setOnLoan(true);
        b.setOnLoan(false);
        c.setOnLoan(true);

        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(c.toString());
    }
}
