package be.kdg.prog1.m5.a04operations;

public class Operations {
    public int numberOne;
    public int numberTwo;
    
    public Operations(int numberOne, int numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    public int sum() {
        return numberOne + numberTwo;
    }
    public int difference() {
        return numberOne - numberTwo;
    }
    public int product() {
        return numberOne * numberTwo;
    }
    public double quotient() {
        return (double)numberOne / numberTwo;
    }

    public String toString() {
        //return String.format("The sum is %d\nThe difference is %d\nThe product is %d\nThe quotient is %2.f", sum(), difference(), product(), quotient());
        return String.format("The sum is %d\nThe difference is %d\nThe product is %d\nThe quotient is %.2f", sum(), difference(), product(), quotient());
    }
}