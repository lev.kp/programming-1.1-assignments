package be.kdg.prog1.m5.a04operations;

import java.util.Scanner;

public class DemoOperations {
    public static void main(String[] args) {

        int a, b;
        Operations op;
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter the first integer: ");
        a = sc.nextInt();
        System.out.print("Please enter the second integer: ");
        b = sc.nextInt();

        op = new Operations(a, b);
        System.out.println(op.toString());
    }
}
