package be.kdg.prog1.m5.a05maximum;

public class Maximum {
    public int one;
    public int two;
    public int three;

    public Maximum() {
        System.out.println("constructor without parameter called");
    }
    public Maximum(int one, int two, int three) {
        System.out.println("int constructor called");
        this.one = one;
        this.two = two;
        this.three = three;
    }
    public Maximum(long one, long two, long three) {
        System.out.println("long constructor called");
        this.one = (int)one;
        this.two = (int)two;
        this.three = (int)three;
    }
    public Maximum(double one, double two, double three) {
        System.out.println("double constructor called");
        this.one = (int)one;
        this.two = (int)two;
        this.three = (int)three;
    }

    public double max() {
        return Math.max(Math.max(one, two), three);
    }
    public double max(int a, int b, int c) {
        System.out.println("int parameters method called");
        return Math.max(Math.max(a, b), c);
    }
    public double max(long a, long b, long c) {
        System.out.println("long parameters method called");
        return Math.max(Math.max(a, b), c);
    }
    public double max(double a, double b, double c) {
        System.out.println("double parameters method called");
        return Math.max(Math.max(a, b), c);
    }
}