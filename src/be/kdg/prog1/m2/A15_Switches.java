package be.kdg.prog1.m2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class A15_Switches {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean sw_1 = true;
        boolean sw_2 = true;
        boolean sw_3 = true;

        try {
            System.out.print("Enter the state of switch 1 (true or false): ");
            sw_1 = sc.nextBoolean();

            System.out.print("Enter the state of switch 2 (true or false): ");
            sw_2 = sc.nextBoolean();

            System.out.print("Enter the state of switch 3 (true or false): ");
            sw_3 = sc.nextBoolean();


        } catch (InputMismatchException e) {
            System.out.println("Invalid input! Please restart and enter either true or false.");
        }

        if (sw_1 && (sw_2 || sw_3) || (sw_2 && sw_3)) {
            System.out.println("At least two switches are turned on.");
        }

        if ((sw_1 && sw_2 && !sw_3) || (sw_1 && !sw_2 && sw_3) || (!sw_1 && sw_2 && sw_3)) {
            System.out.println("Exactly two switches are turned on.");
        }

        if ((!sw_1 && !sw_2) || (!sw_1 && sw_2) || (sw_1 && !sw_2)) {
            System.out.println("At most one switch is turned on.");
        }

        sc.close();
    }
}