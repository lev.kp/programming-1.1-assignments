package be.kdg.prog1.m2;

import java.util.Scanner;

public class A07_ASCIIsum {
    public static void main(String[] args) {

        int sum = 0;
        char[] arr;
        String input;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a string of text: ");
        input = sc.nextLine();
        arr = input.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        System.out.println("sum = " + sum);
        sc.close();
    }
}
