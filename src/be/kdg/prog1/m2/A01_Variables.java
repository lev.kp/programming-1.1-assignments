package be.kdg.prog1.m2;

public class A01_Variables {
    public static void main(String[] args) {

        boolean randBool = false;
        char randChar = 'F';
        byte randByte = 120;
        short randShort = 23984;
        int randInt = 235252255;
        long randLong = 96794622266469L;
        float randFloat = 3.1415926535F;
        double randDouble = 4.2E+87;

        System.out.println(
                    randBool + "\n" +
                    randChar + "\n" +
                    randByte + "\n" +
                    randShort + "\n" +
                    randInt + "\n" +
                    randLong + "\n" +
                    randFloat + "\n" +
                    randDouble
        );

        //final double PI = 3.14;
        //PI = 3.141;
    }
}