package be.kdg.prog1.m2;

public class A06_ASCIItable {
    public static void main(String[] args) {

        for (int asciiNum = 32, counter = 1; asciiNum <= 255; asciiNum++, counter++) {
            System.out.print((char) asciiNum + " " + (asciiNum <= 100 ? " ( " : " (") + asciiNum + ")\t"
                    + (counter % 6 == 0 ? "\n" : ""));
        }
    }
}