package be.kdg.prog1.m2;

import java.util.Random;
import java.util.Scanner;

public class A16_Scramble_Name {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Random random = new Random();
        String name;
        String nameCopy;
        String scrambledName = "";

        System.out.print("Enter your name: ");
        name = sc.nextLine();
        nameCopy = name;

        while (!nameCopy.isEmpty()) {
            int charPosition = random.nextInt(nameCopy.length());
            char c = nameCopy.charAt(charPosition);
            if (c != ' ') {
                scrambledName += c;
            }
            nameCopy = nameCopy.substring(0, charPosition) + nameCopy.substring(charPosition + 1);
        }

        System.out.printf("Hi %s, your scrambled name is %s.", name, scrambledName);
    }
}