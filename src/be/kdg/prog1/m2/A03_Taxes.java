package be.kdg.prog1.m2;

import java.util.Scanner;

public class A03_Taxes {

    public static void main(String[] args) {

        double vat;
        double amount;
        int choice;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the VAT percentage: ");
        vat = sc.nextDouble();

        System.out.print("Enter the amount in €: ");
        amount = sc.nextDouble();

        System.out.print("Make a choice (1 = inclusive, 2 = exclusive): ");
        choice = sc.nextInt();

        if (choice == 1) {
            System.out.println("€" + (amount / (vat / 100 + 1)) + " + " + vat + "% VAT = €" + amount);
        }
        else if (choice == 2) {
            System.out.println("€" + amount + " + " + vat + "% VAT" + " = " + "€" + ((amount / 100) * vat + amount));
        }
        else {
            System.out.println("Invalid choice!");
        }

        sc.close();
    }
}