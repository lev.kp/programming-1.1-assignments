package be.kdg.prog1.m2;

import java.util.Scanner;

public class A09_Pizza {
    public static void main(String[] args) {

        final int PIZZA_PRICE = 800;
        final int TOPPING_PRICE = 50;
        int pizzaAmount = 0;
        int toppingAmount;
        int totalPrice;
        Scanner sc = new Scanner(System.in);

        System.out.print("How many pizzas would you like: ");
        pizzaAmount = sc.nextInt();
        totalPrice = pizzaAmount * PIZZA_PRICE;

        for (int i = 1; i <= pizzaAmount; i++) {

            System.out.print("How many extra toppings for pizza " + i + ": ");
            toppingAmount = sc.nextInt();

            if (toppingAmount != 0) {
               totalPrice += (toppingAmount * TOPPING_PRICE);
           }
        }

        System.out.println("Total price: €" + (double)totalPrice / 100);

        sc.close();
    }
}
