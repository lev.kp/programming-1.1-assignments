package be.kdg.prog1.m2;

import java.util.Scanner;

public class A05_Numbers_v3 {
    public static void main(String[] args) {

        final long MINIMUM_DIVIDENT = 1_000_000_000_000L;
        final long MINIMUM_DIVISOR = 1_000_000_0L;
        long dividend;
        long divisor;
        double quotient;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a 13-digit whole number: ");
        dividend = sc.nextLong();

        System.out.print("Enter an 8-digit whole number: ");
        divisor = sc.nextLong();

        if (dividend <= MINIMUM_DIVIDENT || divisor <= MINIMUM_DIVISOR) {
            System.out.println("This number is too small.");
            return;
        }

        quotient = (double)dividend / divisor;
        System.out.println("The quotient is " + quotient);
        System.out.println("Without the fractional part it's: " + (long)quotient);

        sc.close();
    }
}
