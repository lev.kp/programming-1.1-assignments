package be.kdg.prog1.m2;

import java.util.Scanner;

public class A10_Digits_v1_hard {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String firstInput;
        int result = 0;
        boolean keepRunning = true;

        System.out.println("Enter four strings, or enter -1 as the first string to stop.");

        while (keepRunning) {

            System.out.print("The first string: ");
            firstInput = sc.nextLine();

            if (!firstInput.equals("-1")) {

                result += (int)firstInput.charAt(0) * 1000;

                System.out.print("The second string: ");
                result += (int)sc.nextLine().charAt(0) * 100;

                System.out.print("The third string: ");
                result += (int)sc.nextLine().charAt(0) * 10;

                System.out.print("The fourth string: ");
                result += (int)sc.nextLine().charAt(0);

                System.out.println("The number is " + result);
            }
            else {
                keepRunning = false;
            }
        }

        sc.close();
    }
}