package be.kdg.prog1.m2;

import java.util.Scanner;

public class A04_Numbers_v2 {
    public static void main(String[] args) {

        final long MINIMUM = 100000;
        final long MAXIMUM = 999999;
        long num_1;
        long num_2;
        long product;
        int lastFiveDigits;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a 6-digit whole number: ");
        num_1 = sc.nextLong();

        System.out.print("Enter another 6-digit whole number: ");
        num_2 = sc.nextLong();

        if (num_1 < MINIMUM || num_2 < MINIMUM) {
            System.out.print("One of the numbers is too small.");
        }
        else if (num_1 > MAXIMUM || num_2 > MAXIMUM) {
            System.out.print("One of the numbers is too big.");
        }
        else {
            product = num_1 * num_2;
            System.out.println("The product is " + product);

            lastFiveDigits = (int)(product % 100000);
            if (lastFiveDigits != 0) {
                System.out.println("The 5 final digits are: " + lastFiveDigits);
            }
            else {
                System.out.println("The 5 final digits are: 00000");
            }
        }

        sc.close();
    }
}