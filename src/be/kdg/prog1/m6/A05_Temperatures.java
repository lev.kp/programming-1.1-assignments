package be.kdg.prog1.m6;

import java.util.Scanner;

public class A05_Temperatures {
    public static void main(String[] args) {

        double[] temperatures = new double[7];
        double average = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter 7 temperatures:");

        int i;
        for (i = 0; i < temperatures.length; i++) {
            System.out.printf("Day %d: ", i + 1);
            temperatures[i] = sc.nextDouble();
            average += temperatures[i];
        }

        average /= i;

        System.out.println("\nSummary:\n=================");

        for (int j = 0; j < temperatures.length; j++) {
            System.out.printf("Day %d:\t%.1f\n", (j + 1), temperatures[j]);
        }

        System.out.printf("=================\nAverage: %10.1f", average);









    }

}
