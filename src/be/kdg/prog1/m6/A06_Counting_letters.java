package be.kdg.prog1.m6;

import java.util.Scanner;

public class A06_Counting_letters {
    public static void main(String[] args) {

        final int ASCII_TO_ALPHABET_INDEX = 97;
        final int ASCII_BEFORE_a = 96;
        final int ASCII_AFTER_z = 123;
        int charCount = 0;
        int[] letterCount = new int[26];
        String input;
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter a sentence: ");
        input = sc.nextLine().toLowerCase();

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (ch > ASCII_BEFORE_a && ch < ASCII_AFTER_z) {
                letterCount[ch - ASCII_TO_ALPHABET_INDEX]++;
                charCount++;
            }
        }

        System.out.println("Letter frequencies: ");

        int j;
        char ch;
        for (ch = 97, j = 0; j < letterCount.length; j++, ch++) {
            System.out.printf("%c --> %d times\t%s", ch, letterCount[j], ((j + 1) % 4 == 0 ? "\n" : ""));
        }
        System.out.print("\nTotal amount of letters: " + charCount);
    }
}
