package be.kdg.prog1.m6;

public class A07_Arrays_v2 {
    public static void main(String[] args) {

        StringBuilder[] suits = {
                new StringBuilder("hearts"),
                new StringBuilder("clubs"),
                new StringBuilder("diamonds"),
                new StringBuilder("spades")
        };

        for(StringBuilder sb : suits) {
            System.out.println(sb);
        }
    }
}
