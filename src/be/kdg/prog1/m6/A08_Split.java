package be.kdg.prog1.m6;

import java.util.Scanner;

public class A08_Split {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input;
        String[] arr;

        System.out.print("Enter a sentence: ");
        input = sc.nextLine();
        arr = new String[input.length() - input.replace(" ", "").length() + 1];

        arr = input.split(" ");

        for(String s : arr) {
            System.out.printf("\"%s\" ", s);
        }
    }
}