package be.kdg.prog1.m6;

public class A01_Arrays_v1 {

    public static void main(String[] args) {
        int[] numbers = new int[5];
        float[] stockMarketRates = new float[20];
        boolean[] switches = new boolean[8];
        String[] words = new String[4];

        System.out.printf("%d %f %b %s\n", numbers[1], stockMarketRates[1], switches[1], words[1]);
        System.out.printf("%d %f %b %s",
                numbers[numbers.length - 1],
                stockMarketRates[stockMarketRates.length - 1],
                switches[switches.length - 1],
                words[words.length - 1]
        );
    }
}
