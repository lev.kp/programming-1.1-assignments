package be.kdg.prog1.m4;

public class A04_String_tests {

    public static void main(String[] args) {

        /*

        //Solution: == checks whether the reference of the two strings is equal
        //.equals checks for the actual content

        String one = "abc";
        String two = "abc";
        String three = new String("abc");

        System.out.println(one == two);
        System.out.println(one == three);
        System.out.println(two == three);

        System.out.println(one.equals(two));
        System.out.println(one.equals(three));
        System.out.println(two.equals(three));
        */
    }
}
