package be.kdg.prog1.m4;

import java.util.Scanner;

public class A07_Palindrome {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StringBuilder word = new StringBuilder();
        StringBuilder wordCopy;

        System.out.print("Enter a word: ");
        word.append((sc.next()));
        wordCopy = new StringBuilder(word);

        System.out.println("\"" + wordCopy + "\" is " + ((wordCopy.compareTo(word.reverse())) == 0 ? "" : "not ") + "a palindrome.");
    }
}
