package be.kdg.prog1.m4;

import java.util.Random;
import java.util.Scanner;

public class A06_StringBuilder_v1 {
    public static void main(String[] args) {


        //StringBuilder sb_1 = new StringBuilder();
        StringBuilder sb_2 = new StringBuilder();
        StringBuilder sb_3;
        StringBuilder sb_4 = new StringBuilder();

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter your first name and last name, separated by a space: ");
        StringBuilder input = new StringBuilder(sc.nextLine());

        //I don't want to use StringBuilder here
        String[] initials = input.toString().split(" ");
        for (String initial : initials)
            System.out.print(initial.charAt(0));

        System.out.println();

        System.out.println(sb_2.append(input).reverse());

        sb_3 = new StringBuilder(input);
        for (int i = 0; i < input.length(); i++) {
            if (sb_3.charAt(i) == 'e')
                sb_3.setCharAt(i, 'a');
        }
        System.out.println(sb_3);

        StringBuilder inputCopy = new StringBuilder(input);
        Random r = new Random();

        while (inputCopy.length() != 0) {
            int charPosition = r.nextInt(inputCopy.length());
            char c = inputCopy.charAt(charPosition);
            sb_4.append(c);
            inputCopy.deleteCharAt(charPosition);
        }

        System.out.println(sb_4);
    }
}