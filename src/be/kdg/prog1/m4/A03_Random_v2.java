package be.kdg.prog1.m4;

import java.util.Random;

public class A03_Random_v2 {

    public static void main(String[] args) {
        Random r = new Random();

        for (int i = 0; i < 5; i++)
            System.out.printf("%.2f ", r.nextDouble());

        System.out.println();

        Random seed_1 = new Random(42);
        Random seed_2 = new Random(42);

        for (int j = 0; j < 10; j++)
            System.out.print((seed_1.nextInt(42) + 1) + " " + (seed_2.nextInt(42) + 1) + " ");
    }
}
