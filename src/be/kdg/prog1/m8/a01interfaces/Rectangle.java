package be.kdg.prog1.m8.a01interfaces;

public class Rectangle extends Shape {

    private int height;
    private int width;

    public Rectangle() {

    }

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public void setDimensions(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public double getArea() {
        return width * height;
    }

    public double getPerimeter() {
        return 2 * (height + width);
    }

    public void print() {
        System.out.println(getClass().getSimpleName() + "\n=========");
        System.out.printf("Position: (%d, %d)\nWidth: %d\nHeight: %d\n", getX(), getY(), getWidth(), getHeight());
    }


}
