package be.kdg.prog1.m8.a01interfaces;

public class Car implements Printable {


    public String brand;
    public String model;
    public String licensePlate;

    public Car(String brand, String model, String licensePlate) {
        this.brand = brand;
        this.model = model;
        this.licensePlate = licensePlate;
    }

    public void print() {
        System.out.println(getClass().getSimpleName() + "\n===");
        System.out.printf("Brand: %s\nModel: %s\nLicense plate: %s\n", brand, model, licensePlate);
    }
}