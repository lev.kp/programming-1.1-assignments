package be.kdg.prog1.m8.a01interfaces;

public interface Printable {

    void print();

}
