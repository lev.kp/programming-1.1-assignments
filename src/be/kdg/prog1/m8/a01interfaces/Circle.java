package be.kdg.prog1.m8.a01interfaces;

public class Circle extends Shape{

    private int radius;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Circle() {

    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getArea() {
        return Math.pow(radius, 2) * Math.PI;
    }

    public double getPerimeter() {
        return 2 * radius * Math.PI;
    }

    public void print() {
        System.out.println(getClass().getSimpleName() + "\n======");
        System.out.printf("Position: (%d, %d)\nRadius: %d\n", getX(), getY(), getRadius());
    }
}
