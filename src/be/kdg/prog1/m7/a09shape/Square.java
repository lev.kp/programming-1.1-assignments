package be.kdg.prog1.m7.a09shape;

public class Square extends Rectangle {

    protected int size;

    public Square() {

    }

    public Square(int x, int y) {
        super(x, y);
    }

    public Square(int x, int y, int size) {
        super(x, y);
        this.size = size;
    }

    public Square(int size) {
        this.size = size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public double getArea() {
        return Math.pow(size, 2);
    }

    @Override
    public double getPerimeter() {
        return size * 4;
    }
}
