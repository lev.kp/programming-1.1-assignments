package be.kdg.prog1.m7.a09shape;

public class Circle extends Shape {

    protected int radius;

    public Circle() {

    }

    public Circle(int radius) {
        super(0, 0);
        this.radius = radius;
    }

    public Circle(int x, int y) {
        super(x, y);
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getArea() {
        return Math.pow(radius, 2) * Math.PI;
    }

    public double getPerimeter() {
        return 2 * radius * Math.PI;
    }
}