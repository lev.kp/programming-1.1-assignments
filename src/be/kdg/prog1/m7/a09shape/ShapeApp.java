package be.kdg.prog1.m7.a09shape;

public class ShapeApp {

    public static void main(String[] args) {
        Circle c = new Circle(5);
        Rectangle r = new Rectangle(2,3, 4, 5);
        Square s = new Square(10);
        System.out.println(c.toString());
        System.out.println(r.toString());
        System.out.printf(s.toString());


        //A10

        Shape[] sh_arr = new Shape[3];
        sh_arr[0] = new Circle(10);
        sh_arr[1] = new Rectangle(1, 1, 5, 10);
        sh_arr[2] = new Square(10);

        for (Shape i : sh_arr) {
            System.out.println(i.toString());
        }


    }
}
