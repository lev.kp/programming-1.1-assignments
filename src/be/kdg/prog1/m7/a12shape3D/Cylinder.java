package be.kdg.prog1.m7.a12shape3D;

public class Cylinder extends Shape3D {

    protected double radius = 1.0;
    protected double length = 1.0;

    public Cylinder() {

    }

    public Cylinder(String color, double radius, double length) {
        super(color);
        this.radius = radius;
        this.length = length;
    }

    public double surface() {
        return 2 * Math.PI * radius * (length + radius);
    }

    public double volume() {
        return Math.PI * Math.pow(radius, 2) * length;
    }
}
