package be.kdg.prog1.m7.a12shape3D;

public class Cube extends Shape3D {

    protected double edge = 1.0;

    public Cube() {

    }

    public Cube(String color, double edge) {
        super(color);
        this.edge = edge;
    }

    public Cube(double edge) {
        this.edge = edge;
    }

    public double surface() {
        return 6 * Math.pow(edge, 2);
    }

    public double volume() {
        return Math.pow(edge, 3);
    }
}
