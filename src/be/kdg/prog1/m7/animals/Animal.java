package be.kdg.prog1.m7.animals;

public class Animal {
    protected String name;
    protected String breed;
    protected String color;
    protected String tagLine;

    public Animal(String name, String breed, String color, String tagLine) {
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.tagLine = tagLine;

        if (this instanceof Rabbit) {
            this.tagLine = "I'm an ice rabbit";
        }
        else {
            this.tagLine = "???";
        }

    }

    public Animal() {

    }

    public String getTagLine() {
        return tagLine;
    }

    @Override
    public String toString() {
        return String.format("Name: %s", name);
    }
}
