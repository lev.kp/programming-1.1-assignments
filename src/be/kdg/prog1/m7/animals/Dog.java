package be.kdg.prog1.m7.animals;

public class Dog extends Animal {

    private final String chipNumber;

    public Dog(String name, String breed, String color, String tagLine, String chipNumber) {
        super(name, breed, color, tagLine);
        this.chipNumber = chipNumber;
    }

}
