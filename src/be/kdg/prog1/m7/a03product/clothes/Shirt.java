package be.kdg.prog1.m7.a03product.clothes;

import be.kdg.prog1.m7.a03product.Product;

public class Shirt extends Product {

    private String gender;
    private String author;

    public Shirt(String code, String description, double price, String gender, String author) {
        super(code, description, price);
        this.gender = gender;
        this.author = author;
    }

    public String getGender() {
        return gender;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Shirt{" +
                "gender='" + getGender() + '\'' +
                ", author='" + getAuthor() + '\'' +
                ", code='" + getCode() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", price=" + getPrice() +
                '}';
    }
}