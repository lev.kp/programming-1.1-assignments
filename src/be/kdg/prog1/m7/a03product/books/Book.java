package be.kdg.prog1.m7.a03product.books;

import be.kdg.prog1.m7.a03product.Product;

public class Book extends Product {

    private String title;
    private String author;

    public Book(String code, String description, double price, String title, String author) {
        super(code, description, price);
        this.title = title;
        this.author = author;
    }


    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public double getVat() {
        return 0.6;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + getTitle() + '\'' +
                ", author='" + getAuthor() + '\'' +
                ", code='" + getCode() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", price=" + getPrice() +
                '}';
    }
}
