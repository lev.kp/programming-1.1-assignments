package be.kdg.prog1.m3;

import java.util.Scanner;

public class A06_Days_in_Month {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int monthNumber;
        int year;

        System.out.print("Enter a month number (1 = january): ");
        monthNumber = sc.nextInt();

        System.out.print("Enter a year (4 digits): ");
        year = sc.nextInt();


        //To also print the names of the months: insert a print under each case with name and number of days
        switch (monthNumber) {

            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println("The number of days in month " + monthNumber + " is 31.");
                break;

            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println("The number of days in month " + monthNumber + " is 30.");
                break;

            case 2:
                System.out.println("The number of days in month " + monthNumber + " is "
                        + (year % 4 == 0 ? "29." : "28."));
                break;

            default:
                System.out.println("Enter a number from 1 to 12.");
        }

        sc.close();
    }
}
