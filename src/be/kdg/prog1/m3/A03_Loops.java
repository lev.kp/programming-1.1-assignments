package be.kdg.prog1.m3;

public class A03_Loops {
    public static void main(String[] args) {

        whileLoops();
        doWhileLoops();
        forLoops();
    }

    static void whileLoops() {

        int i = 120;
        while (i != 99) {
            System.out.print(i-- + " ");
        }

        System.out.println();

        int j = 0;
        while (j < 50) {
            System.out.print(j + " ");
            j += 3;
        }

        System.out.println();
        int exp = 1;
        while (exp < 10000) {
            System.out.print(exp + " ");
            exp *= 5;
        }

        System.out.println();

        char ch = 'a';
        while (ch <= 'z') {
            System.out.print(ch++ + " ");
        }

        System.out.println();
    }

    static void doWhileLoops() {

        int i = 120;
        do {
            System.out.print(i-- + " ");
        } while (i != 99);

        System.out.println();

        int j = 0;
        do {
            System.out.print(j + " ");
            j += 3;
        } while(j < 50);

        System.out.println();

        int exp = 1;
        do {
            System.out.print(exp + " ");
            exp *= 5;
        } while(exp < 10000);

        System.out.println();

        char ch = 'a';
        do {
            System.out.print(ch++ + " ");
        } while(ch <= 'z');

        System.out.println();
    }

    static void forLoops() {

        for (int i = 120; i >= 100; i--) {
            System.out.print(i + " ");
        }

        System.out.println();

        for (int i = 0; i < 50; i += 3) {
            System.out.print(i + " ");
        }

        System.out.println();

        for (int exp = 1; exp < 10000; exp *= 5) {
            System.out.print(exp + " ");
        }

        System.out.println();

        for (char ch = 'a'; ch <= 'z' ; ch++) {
            System.out.print(ch + " ");
        }

        System.out.println();
    }
}
