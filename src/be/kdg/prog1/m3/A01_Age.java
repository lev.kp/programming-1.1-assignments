package be.kdg.prog1.m3;

import java.util.Scanner;

public class A01_Age {
    public static void main(String[] args) {

        int age = 0;
        Scanner sc = new Scanner(System.in);

        System.out.print("How old are you?");
        age = sc.nextInt();

        if (age < 2) {
            System.out.println("Baby");
        }
        else if(age >= 2 && age <= 12) {
            System.out.println("Child");
        }
        else if (age >= 13 && age <= 17) {
            System.out.println("Teenager");
        }
        else {
            System.out.println("Adult");
        }

        sc.close();
    }
}
