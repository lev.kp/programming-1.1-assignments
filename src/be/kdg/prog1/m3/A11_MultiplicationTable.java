package be.kdg.prog1.m3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class A11_MultiplicationTable {


    public static void main(String[] args) {

        int num = 0;
        boolean inputError;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Enter a number between 1 and 30: ");
            inputError = false;

            try {

                num = sc.nextInt();

                if (num < 1 || num > 30) {
                    inputError = true;
                }

            } catch (InputMismatchException e) {
                inputError = true;
                sc.nextLine();
            }

        } while(inputError);

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= num; j++) {
                System.out.printf("|%3d%s", i * j, (j == num ? "|\n" : ""));
            }
        }
    }
}