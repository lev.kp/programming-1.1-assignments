package be.kdg.prog1.m3;

import java.util.Scanner;

public class A10_ASCII_box {
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        char ch;
        int height;
        int width;
        boolean inputError;

        System.out.println("We'll draw an ASCII box using a character and dimensions of your choice.");
        System.out.print("Enter a character: ");
        ch = sc.nextLine().charAt(0);

        do {
            System.out.print("Enter the width (2..60): ");
            width = sc.nextInt();
            if (width < 2 || width > 60) {
                System.out.println("Please enter a value between 2 and 60.");
                inputError = true;
            }
            else {
                inputError = false;
            }
        } while(inputError);

        do {
            System.out.print("Enter the height (2..20): ");
            height = sc.nextInt();
            if (height < 2 || height > 20) {
                System.out.println("Please enter a value between 2 and 20.");
                inputError = true;
            }
            else {
                inputError = false;
            }
        } while(inputError);

       // task_1(ch, width, height);
        System.out.println();
        task_2(ch, width, height);


    }

    static void task_1(char ch, int width, int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(ch + (j == width - 1 ? "\n" : ""));
            }
        }
    }

    static void task_2(char ch, int width, int height) {
        for (int i = 0; i < height; i++) {
            System.out.print(ch);
            for (int j = 0; j < width - 2; j++) {
                System.out.print(i == 0 || i == height - 1 ? ch : ' ');
            }
            System.out.print(ch + "\n");
        }
    }
}
