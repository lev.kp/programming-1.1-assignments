package be.kdg.prog1.m3;

public class A07_Hexadecimal {
    public static void main(String[] args) {

        char ch = 'A';
        for (int i = 0; i < 16; i++) {
            System.out.print(i < 10 ? i + " " : ch++ + " ");
        }
    }
}
